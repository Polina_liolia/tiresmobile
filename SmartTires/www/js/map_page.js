
//binding event on map page:

$("#service").bind('pageshow', initMap);
$("#service").bind('pagehide', cleanDirectionsPanel);
$("#show_map").on("vmousedown", function () {
    localStorage.setItem("map_opened", "true");
});

function initMap(){
    var heightScreen = $(window).height();
    var widthScreen = $(window).width();
    var headerHeight = $("#service>div[data-role=header]").height();
    var footerHeight = $("#service>div[data-role=footer]").height();

    $('#map').css({'position':'absolute', 'width':widthScreen,
        'height':heightScreen - headerHeight - footerHeight,
        'top': headerHeight+2, 'left': 0});

    var basic_latlng = new google.maps.LatLng(myLat, myLong);

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: basic_latlng,
         cooperative: true
    });
    //to resize map after screen orientation was changed:
    $(window).on("orientationchange",function()
    {
        var center = map.getCenter();
        var heightScreen = $(window).height();
        var widthScreen = $(window).width();
        var headerHeight = $("#service>div[data-role=header]").height();
        var footerHeight = $("#service>div[data-role=footer]").height();
        $('#map').css({'position':'absolute', 'width':widthScreen,
            'height':heightScreen - headerHeight - footerHeight,
            'top': headerHeight+2, 'left': 0});
        map.setCenter(center);
    });



    //creating objects to manage directions calculating:
    var dirService = new google.maps.DirectionsService();
    var dirRenderer = new google.maps.DirectionsRenderer();
    dirRenderer.setMap(map);
    dirRenderer.setPanel(document.getElementById('directionsPanel'));


    //to mark user's current position:
    var truck_marker = new google.maps.Marker({
        position: {lat: myLat, lng: myLong},
        icon: "img/truck.png",
        map: map,
        animation: google.maps.Animation.DROP
    });
    //to show user's current position in the center of the screen:
    $('#my_coord').on('vmousedown', function () {
        map.setCenter(basic_latlng);
        truck_marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function () {
            truck_marker.setAnimation(null);
        }, 1000);
    });


    //getting from LocalStorage JSON file with truckforces names and addresses:
    var truckforces = [];
    var points = [];
    var infowindows = [];
    var data = localStorage.getItem("truckforces");
    if (data){
        data = JSON.parse(data);
        $(data).each(function (i) {
           var contentString = data[i].name + '<br>' + data[i].address + '<br>' +
               '<a href="tel:'+ data[i].phone + '">' + data[i].phone + '</a>';
           var marker = new google.maps.Marker({
                position: {lat: data[i].lat, lng: data[i].lng},
                label: data[i].name
            });
            var truckforce_latlng = new google.maps.LatLng(data[i].lat, data[i].lng);
            //calculating a distance between truck and current truckforce:
            var distance = (google.maps.geometry.spherical.computeDistanceBetween(basic_latlng, truckforce_latlng) / 1000).toFixed(2);
            var infowindow = new google.maps.InfoWindow({
                content: contentString //contentElement
            });
            infowindows.push(infowindow);
            marker.addListener('click', function () {
                for (var i = 0; i < infowindows.length; i++)
                    infowindows[i].close();
                infowindow.open(map, marker);
                calcRoute(dirService, dirRenderer, basic_latlng, marker.position);
            });
            points.push(marker);
            truckforces.push(new Truckforce(data[i].name, data[i].address, data[i].phone, data[i].worktime, data[i].lat, data[i].lng, Number(distance)));
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, points,
            {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
                maxZoom: 8
            });
        $('#service>div[data-role=footer]').text("").append("<ul/>");
        //sorting truckforces by the distance from client's truck:
        truckforces = truckforces.sort(function (a, b) {
            if (a.distance > b.distance)
                return 1;
            if (a.distance == b.distance)
                return 0;
            return -1;
        });
        //appending sorted list to footer:
        $(truckforces).each(function (i) {
            $('<li/>')
                .css("cursor", "pointer")
                .text(truckforces[i].distance + "км " + truckforces[i].name)
                .appendTo('#service>div[data-role=footer]>ul')
                .on('vclick', function () {
                    var pointIndex = -1;
                    //searching for the map point for clicked truckforce:
                    $(points).each(function(j){
                        if(points[j].label == truckforces[i].name)
                        {
                            pointIndex = j;
                            return false;
                        }
                    });
                    if (pointIndex != -1)
                    {
                        for (var j = 0; j < infowindows.length; j++)
                            infowindows[j].close();
                        map.setCenter(points[pointIndex].position);
                        points[pointIndex].setAnimation(google.maps.Animation.BOUNCE);
                        calcRoute(dirService, dirRenderer, basic_latlng, points[pointIndex].position);
                        setTimeout(function () {
                            points[pointIndex].setAnimation(null);
                            infowindows[pointIndex].open(map, points[pointIndex]);
                        }, 500);
                    }
                });
        });
    }
    else{
        var map_opened = localStorage.getItem("map_opened");
        if (map_opened == "true") {
            localStorage.setItem("map_opened", "false");
            $("#loadFail div[role=main]").text("Ошибка загрузки данных о сервисных станциях. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
            $.mobile.changePage("#loadFail", {role: "dialog"});
            $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
                .on("vmousedown", function (event) {
                $("#loadFail div[role=main]").text("");
                event.preventDefault();
                event.stopImmediatePropagation();
                $.mobile.changePage("#main");
            });
        }
    }
}

function cleanDirectionsPanel() {
    console.log("routes cleaned!");
    $("#directionsPanel").text("");
}