

//actions on Pressure measure page:
$("#pressure").bind('pageshow', function() {
    //setting initial status of form elements:
    $( "#size" ).selectmenu( "option", "disabled", true );
    // $("input[value=single]").checkboxradio( "option", "disabled", true );
    // $("input[value=double]").checkboxradio( "option", "disabled", true );
    $( "#load_index" ).selectmenu( "option", "disabled", true );

    var truck_tires_catalog  = localStorage.getItem("truck_tires");
    if (truck_tires_catalog) {
        truck_tires_catalog = JSON.parse(truck_tires_catalog);
    }
    else{
        $("#loadFail div[role=main]").text("Ошибка загрузки каталога грузовых шин. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
        $.mobile.changePage( "#loadFail", { role: "dialog" } );
        $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
            .on("vmousedown", function(event){
            $("#loadFail div[role=main]").text("");
            event.preventDefault();
            $.mobile.changePage( "#main");
        });
        return;
    }

    var trailer_tires_catalog  = localStorage.getItem("trailer_tires");
    if (trailer_tires_catalog) {
        trailer_tires_catalog = JSON.parse(trailer_tires_catalog);
    }
    else{
        $("#loadFail div[role=main]").text("Ошибка загрузки каталога шин для прицепов. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
        $.mobile.changePage( "#loadFail", { role: "dialog" } );
        $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
            .on("vmousedown", function(event){
            $("#loadFail div[role=main]").text("");
            event.preventDefault();
            event.stopImmediatePropagation();
            $.mobile.changePage( "#main");
        });
        return;
    }


    //a function to filter array and live only unique elements
    function unique(arr) {
        var obj = {};

        for (var i = 0; i < arr.length; i++) {
            var str = arr[i];
            obj[str] = true; // запомнить строку в виде свойства объекта
        }

        return Object.keys(obj); // или собрать ключи перебором для IE8-
    }


    var source = []; //source catalog according to user's tire type choice

    //user's choice data:
    var type, sz, instal, load_index, axleLoad;
    var catalog_objs = []; //array to save all tires from catalog with type and size, chosen by user

    //adding events on pressure_data forms fields change
    $("input[name=truckType]").change(function (event) {
        event.stopImmediatePropagation();
        $("input[name=truckType]").parent().css('border-color','#8c8c8c');
        type = $("input[name=truckType]:checked").val();
        localStorage.setItem("type", type);
        if (!type)
            return;
        //getting all available tire sizes for selected transport type:
        source = type == 'truck' ? truck_tires_catalog : trailer_tires_catalog;
        $("#size option").remove(':not(:first-child)'); //if other tire type had already been chosen before
        var sizesList = [];
        $(source).each(function () {
            sizesList.push($(this).prop('tireSize'));
        });
        sizesList = unique(sizesList);
        //adding available tire sizes to the select as options:
        $(sizesList).each(function () {
            $("<option/>").text(this).val(this).appendTo($("#size"));
        });
        $("#size").selectmenu( "option", "disabled", false );
        $("#size").selectmenu('refresh', true);

        //clearing all data, that could be chosen before:
        $("input[value=single]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("input[value=double]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("#load_index option").remove(':not(:first-child)');
        $("#load_index").selectmenu('refresh');
        sz = instal = load_index = undefined;
    });

    //adding event listener on size select to make load properties available:
    $("#size").change(function(event) {
        $("#size").parent().css('border-color','#8c8c8c');
        event.stopImmediatePropagation();
        sz = $("#size option:selected").val();
        localStorage.setItem("size", sz);
        //finding all tires from catalog with type and size, chosen by user:
        catalog_objs = source.filter(function (item) {
            return item.tireSize == sz;
        });

        //checking, if different types of installation are available:
        var flag_single = false,
            flag_double = false;
        $(catalog_objs).each(function () {
            if (this.instal == "single")
                flag_single = true;
            else if (this.instal == "double")
                flag_double = true;
        });
        $("input[value=single]").checkboxradio( "option", "disabled", !flag_single )
                .prop('checked', false)
                .checkboxradio('refresh');
        $("input[value=double]").checkboxradio("option", "disabled", !flag_double)
                .prop('checked', false)
                .checkboxradio('refresh');
        $("#load_index option").remove(':not(:first-child)');
        $("#load_index").selectmenu('refresh');
        instal = load_index = undefined;
    });

    $("input[name=installation]").change(function (event) {
        $("input[name=installation]").parent().css('border-color','#8c8c8c');
        event.stopImmediatePropagation();
        instal = $("input[name=installation]:checked").val();
        localStorage.setItem("instal", instal);
        //getting all values of load indexes for this size:
        var load_indexes = [];
        $(catalog_objs).each(function () {
            if (this.instal == instal)
                load_indexes.push(this.loadIndex);
        });
        $("#load_index option").remove(':not(:first-child)'); //if other install types had already been chosen before
        //adding available load indexes to the select as options:
        $(load_indexes).each(function () {
            $("<option/>").text(this).val(this).appendTo($("#load_index"));
        });
        $("#load_index").selectmenu( "option", "disabled", false );
        $("#load_index").selectmenu('refresh', true);
    });

    $("#load_index").change(function (event) {
        $("#load_index").parent().css('border-color','#8c8c8c');
        event.stopImmediatePropagation();
        load_index = $("#load_index option:selected").val();
        localStorage.setItem("load_index", load_index);
    });

    $("input[name=axleLoad]").on("input", function () {
        $("#axleLoad").parent().css('border-color','#8c8c8c');
        axleLoad = Number($("input[name=axleLoad]").val());
        localStorage.setItem("axleLoad", axleLoad);
    });

    //two way to get optimal pressure calculated:
    $("#get_result").on("vclick", calculate_pressure);
    // var form = document.forms.pressure_data;
    // form.addEventListener("submit", calculate_pressure);
    $("#pressure_data").submit(calculate_pressure);
    function calculate_pressure (event) {
        event.stopImmediatePropagation();
        type = localStorage.getItem("type");
        sz = localStorage.getItem("size");
        instal = localStorage.getItem("instal");
        load_index = localStorage.getItem("load_index");
        axleLoad = Number(localStorage.getItem("axleLoad"));
        if (!type || !sz || !instal || !load_index || !axleLoad)
        {
            event.preventDefault();
            !type ? $("input[name=truckType]").parent().css('border-color','red') :
                $("input[name=truckType]").parent().css('border-color','#8c8c8c');
            !sz ? $("#size").parent().css('border-color','red') :
                $("#size").parent().css('border-color','#8c8c8c');
            !instal ? $("input[name=installation]").parent().css('border-color','red') :
                $("input[name=installation]").parent().css('border-color','#8c8c8c');
            !load_index ? $("#load_index").parent().css('border-color','red') :
                $("#load_index").parent().css('border-color','#8c8c8c');
            !axleLoad ? $("#axleLoad").parent().css('border-color','red') :
                $("#axleLoad").parent().css('border-color','#8c8c8c');
            return;
        }
        var optiPressure = 0;
        var usersTire = new Tire(type, sz, instal, load_index);
        var catalogTire = 0;
        catalog_objs.forEach(function (item, i) {
            if (item.instal == usersTire.instal &&
                Number(item.loadIndex) == Number(usersTire.loadIndex)) {
                catalogTire = item;
                return false;
            }
        });

        if (axleLoad > Number(catalogTire.loadMax))
        {
            event.preventDefault();
            event.stopImmediatePropagation();
            $("#overLoad div[role=main]").text("Ваша нагрузка на ось превышает допустимое значение (" +
                catalogTire.loadMax + " кг)");
            $.mobile.changePage( "#overLoad", { role: "dialog" } );
            return;
        }

        //defining an optimal pressure:
        var prev_pressure = 5, //initialized with min pressure
            prev_load = 0;
        for (var key in catalogTire.tirePressure)
        {
            var current_pressure = Number(key);
            var current_load = Number(catalogTire.tirePressure[key]);
            if (prev_load <= axleLoad && current_load >= axleLoad)
            {
                var mid_load = (prev_load + current_load) / 2;
                optiPressure = (axleLoad < mid_load) ? prev_pressure : current_pressure;
                localStorage.setItem("optiPressure", optiPressure);
                break;
            }
            prev_pressure = current_pressure;
            prev_load = current_load;
        }

        event.preventDefault();
        $.mobile.changePage( "#result", { role: "dialog" } );
        //filling in form results:
        $("#result p:first-of-type>span ")
            .text(type == 'truck' ? "Грузовые автомобили, тягачи, автобусы" :
                type == 'trailer' ? 'Прицепы и полуприцепы' : 'Не определен');
        $("#result p:nth-of-type(2)>span").text(sz ? sz : "не определен");
        $("#result p:nth-of-type(3)>span")
            .text(instal == 'single' ? 'одиночная' : instal == 'double' ? 'сдвоенная'
                    : 'не определена');
        $("#result p:nth-of-type(4)>span")
            .text(load_index ? load_index : "не определен");
        $("#result p:nth-of-type(5)>span").text(axleLoad ? axleLoad : "не определена");
        $("#result p:nth-of-type(6)>span").text(localStorage.getItem("optiPressure") != "" ? localStorage.getItem("optiPressure") : "не определено");
        //when result dialog has been closed:
        $("#result").bind("pagehide",function() {
            //clearing data in localStorage:
            localStorage.setItem("type", "");
            localStorage.setItem("size", "");
            localStorage.setItem("instal", "");
            localStorage.setItem("load_index", "");
            localStorage.setItem("axleLoad", "");
            localStorage.setItem("optiPressure", "");

            var launches = localStorage.getItem("launches");
            launches = launches ? Number(launches) : 0;
            var users_reaction = localStorage.getItem("users_reaction");
            if (launches >= 5 && (users_reaction == "later" || !users_reaction)) {
                $.mobile.changePage("#rate", {role: "dialog"});
                $("#yes_rate").on("vclick", function (event) {
                    event.stopImmediatePropagation();
                    localStorage.setItem("users_reaction", "rated");
                    var platform = localStorage.getItem("platform");
                    if (platform == "Android")
                        $("#yes_rate").attr("href", "https://play.google.com/store/apps/details?id=com.fortruckdrivers.SmartTires");
                    else
                        $("#yes_rate").attr("href", "http://windowsphone.com.ua/");
                    $("#rate").dialog("close");
                });
                $("#rate_later").on("vclick", function (event) {
                    event.stopImmediatePropagation();
                    localStorage.setItem("users_reaction", "later");
                    $("#rate").dialog("close");
                });
                $("#never_rate").on("vclick", function (event) {
                    event.stopImmediatePropagation();
                    localStorage.setItem("users_reaction", "never_rate");
                    $("#rate").dialog("close");
                });
            }
        });

        // clearing form for next query:
        $("input[name=truckType]").prop('checked', false)
            .checkboxradio('refresh');
        $("#size option").remove(':not(:first-child)');
        $( "#size" ).selectmenu( "option", "disabled", true )
            .selectmenu('refresh');
        $("input[value=single]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("input[value=double]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("#load_index option").remove(':not(:first-child)');
        $("#load_index").selectmenu( "option", "disabled", true )
            .selectmenu('refresh');
        $("input[name=axleLoad]").val("").textinput('refresh');

        $("input[name=truckType]").parent().css('border-color','#8c8c8c');
        $("#size").parent().css('border-color','#8c8c8c');
        $("input[name=installation]").parent().css('border-color','#8c8c8c');
        $("#load_index").parent().css('border-color','#8c8c8c');
        $("#axleLoad").parent().css('border-color','#8c8c8c');
        //cleaning data:
        type = sz = instal = load_index = axleLoad = undefined;
        source = catalog_objs = [];
    }
});




