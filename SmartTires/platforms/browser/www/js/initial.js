var myLat = 0, myLong = 0;

var watchID = null;

//adding ability to swipe right
$(document).off('swiperight').on('swiperight', function(){
    history.back();
});

// Wait for device API libraries to load
$(document).ready(function(){
    // on mobile device, we must wait the 'deviceready' event fired by cordova
    if(/(android)/i.test(navigator.userAgent)) {
        document.addEventListener('deviceready', onDeviceReady, false);
    } else {
        onDeviceReady();
    }
});


// device APIs are available
function onDeviceReady() {
    document.removeEventListener('deviceready', onDeviceReady, false);
    var options = { timeout: 300000 };
    watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    //navigator.geolocation.clearWatch(watchID);
    $("#yes_close").on("vmousedown", function () {
        navigator.app.exitApp();
    });
    $("#dont_close").on("vmousedown", function () {
        $('#exit').dialog('close');
    });

    //getting amount of app launches:
    var launches = localStorage.getItem("launches");
    launches = launches != "" ? Number(launches) : 0;
    launches++;
    localStorage.setItem("launches", launches);

    //getting platform info:
    var platform = device.platform;
    localStorage.setItem("platform", platform);

    //checking network connection:
    var state = checkConnection();
    if (state == 'No network connection')
        $.mobile.changePage( "#noInternetConnection", { role: "dialog" } );

    //start showing addware:
    adMobLauncher();

}

// onSuccess Geolocation
function onSuccess(position) {
    var element = document.getElementById('geolocation');
    myLat = position.coords.latitude;
    myLong = position.coords.longitude;
}


// onError Callback receives a PositionError object
function onError(error) {
    $("#loadFail div[role=main]").text("Ошибка определения вашей геопозиции: " + error.message);
    $.mobile.changePage( "#loadFail", { role: "dialog" } );
    $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
        .on("vmousedown", function(event){
        $("#loadFail div[role=main]").text("");
        event.preventDefault();
        event.stopImmediatePropagation();
        $.mobile.changePage( "#main");
    });
}

//getting json documents from server and putting all data to local storage
$(document).on('pageinit', "[data-role=page]", function() {

   // $("#map_actions").popup( "close" );

//loading updated data from server and saving in local storage:
    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/truck_tires_catalog",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("truck_tires", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/trailer_tires_catalog",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("trailer_tires", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/truckforces",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("truckforces", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/truckforces",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("truckforces", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/speed_indexes",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("speed_indexes", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/load_indexes",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("load_indexes", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/pressure_units",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("pressure_units", dataJSON);
        }
    });
});
