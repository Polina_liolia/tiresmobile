//converting pressure units:
$("#convert_pressure").bind('pageshow', function () {
  var pressure_units = localStorage.getItem("pressure_units");
  if (pressure_units)
  {
      pressure_units = JSON.parse(pressure_units);
      //arrays for lists of different types of pressure units:
      var kPa = [];
      var bar = [];
      var psi = [];
      var kg_sm = [];
      //populating arrays:
      for (var i = 0; i < pressure_units.length; i++)
      {
          kPa.push(pressure_units[i].kPa);
          bar.push(pressure_units[i].bar);
          psi.push(pressure_units[i].psi);
          kg_sm.push(pressure_units[i].kg_sm2)
      }
      if ($("#kpa option").length <= 1 )//if select is empty
      {
          //adding lists from arrays to selects:
          $(kPa).each(function () {
              $("<option/>").text(this).val(this).appendTo($("#kpa"));
          });
          $("#kpa").selectmenu('refresh', true);
      }
      if ($("#bar option").length <= 1 )//if select is empty
      {
          $(bar).each(function () {
              $("<option/>").text(this).val(this).appendTo($("#bar"));
          });
          $("#bar").selectmenu('refresh', true);
      }
      if ($("#psi option").length <= 1 )//if select is empty
      {
          $(psi).each(function () {
              $("<option/>").text(this).val(this).appendTo($("#psi"));
          });
          $("#psi").selectmenu('refresh', true);
      }
      if ($("#kg_sm option").length <= 1 )//if select is empty
      {
          $(kg_sm).each(function () {
              $("<option/>").text(this).val(this).appendTo($("#kg_sm"));
          });
          $("#kg_sm").selectmenu('refresh', true);
      }
      //change event handlers for every select:
      $("#kpa").change(function(event) {
          event.stopImmediatePropagation();
          var kpa = $("#kpa option:selected").val();
          //finding object with kpa, chosen by user:
          var obj = pressure_units.filter(function (item) {
              return item.kPa == kpa;
          });
          $("#bar option").removeAttr('selected');
          $("#bar option[value='" + obj[0].bar + "']").attr("selected", "selected");
          $("#bar").selectmenu('refresh', true);
          $("#psi option").removeAttr('selected');
          $("#psi option[value='" + obj[0].psi + "']").attr("selected", "selected");
          $("#psi").selectmenu('refresh', true);
          $("#kg_sm option").removeAttr('selected');
          $("#kg_sm option[value='" + obj[0].kg_sm2 + "']").attr("selected", "selected");
          $("#kg_sm").selectmenu('refresh', true);
      });

      $("#bar").change(function(event) {
          event.stopImmediatePropagation();
          var bar = $("#bar option:selected").val();
          //finding object with bar, chosen by user:
          var obj = pressure_units.filter(function (item) {
              return item.bar == bar;
          });
          $("#kpa option").removeAttr('selected');
          $("#kpa option[value='" + obj[0].kPa + "']").attr("selected", "selected");
          $("#kpa").selectmenu('refresh', true);
          $("#psi option").removeAttr('selected');
          $("#psi option[value='" + obj[0].psi + "']").attr("selected", "selected");
          $("#psi").selectmenu('refresh', true);
          $("#kg_sm option").removeAttr('selected');
          $("#kg_sm option[value='" + obj[0].kg_sm2 + "']").attr("selected", "selected");
          $("#kg_sm").selectmenu('refresh', true);
      });

      $("#psi").change(function(event) {
          event.stopImmediatePropagation();
          var psi = $("#psi option:selected").val();
          //finding object with psi, chosen by user:
          var obj = pressure_units.filter(function (item) {
              return item.psi == psi;
          });
          $("#kpa option").removeAttr('selected');
          $("#kpa option[value='" + obj[0].kPa + "']").attr("selected", "selected");
          $("#kpa").selectmenu('refresh', true);
          $("#bar option").removeAttr('selected');
          $("#bar option[value='" + obj[0].bar + "']").attr("selected", "selected");
          $("#bar").selectmenu('refresh', true);
          $("#kg_sm option").removeAttr('selected');
          $("#kg_sm option[value='" + obj[0].kg_sm2 + "']").attr("selected", "selected");
          $("#kg_sm").selectmenu('refresh', true);
      });

      $("#kg_sm").change(function(event) {
          event.stopImmediatePropagation();
          var kg_sm = $("#kg_sm option:selected").val();
          //finding object with kg_sm, chosen by user:
          var obj = pressure_units.filter(function (item) {
              return item.kg_sm2 == kg_sm;
          });
          $("#kpa option").removeAttr('selected');
          $("#kpa option[value='" + obj[0].kPa + "']").attr("selected", "selected");
          $("#kpa").selectmenu('refresh', true);
          $("#bar option").removeAttr('selected');
          $("#bar option[value='" + obj[0].bar + "']").attr("selected", "selected");
          $("#bar").selectmenu('refresh', true);
          $("#psi option").removeAttr('selected');
          $("#psi option[value='" + obj[0].psi + "']").attr("selected", "selected");
          $("#psi").selectmenu('refresh', true);
      });
  }
  else
  {
      $("#loadFail div[role=main]").text("Ошибка загрузки данных для конвертера. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
      $.mobile.changePage("#loadFail", {role: "dialog"});
      $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
          .on("vmousedown", function (event) {
          $("#loadFail div[role=main]").text("");
          event.preventDefault();
          event.stopImmediatePropagation();
          $.mobile.changePage("#converters");
      });
  }
});

//clearing all selects, made by user:
$("#convert_pressure").bind('pagehide', function () {
    $("#kpa option").removeAttr('selected');
    $("#kpa").selectmenu('refresh', true);
    $("#bar option").removeAttr('selected');
    $("#bar").selectmenu('refresh', true);
    $("#psi option").removeAttr('selected');
    $("#psi").selectmenu('refresh', true);
    $("#kg_sm option").removeAttr('selected');
    $("#kg_sm").selectmenu('refresh', true);
});

//converting speed indexes:
$("#convert_speed").bind('pageshow', function () {
    var speed_indexes = localStorage.getItem("speed_indexes");
    if (speed_indexes)
    {
        speed_indexes = JSON.parse(speed_indexes);
        //arrays for lists of different types of speed indexes:
        var speed_index_arr = [];
        var speed_arr = [];
        //populating arrays:
        for (var i = 0; i < speed_indexes.length; i++)
        {
            speed_index_arr.push(speed_indexes[i].speed_index);
            speed_arr.push(speed_indexes[i].speed);
        }
        if($("#speed_index option").length <= 1){
            //adding lists from arrays to selects:
            $(speed_index_arr).each(function () {
                $("<option/>").text(this).val(this).appendTo($("#speed_index"));
            });
            $("#speed_index").selectmenu('refresh', true);
        }
        if($("#km_hour option").length <= 1) {
            $(speed_arr).each(function () {
                $("<option/>").text(this).val(this).appendTo($("#km_hour"));
            });
            $("#km_hour").selectmenu('refresh', true);
        }

        //change event handlers for every select:
        $("#speed_index").change(function(event) {
            event.stopImmediatePropagation();
            var speed_index = $("#speed_index option:selected").val();
            //finding object with speed index, chosen by user:
            var obj = speed_indexes.filter(function (item) {
                return item.speed_index == speed_index;
            });
            $("#km_hour option").removeAttr('selected');
            $("#km_hour option[value='" + obj[0].speed + "']").attr("selected", "selected");
            $("#km_hour").selectmenu('refresh', true);
        });

        $("#km_hour").change(function(event) {
            event.stopImmediatePropagation();
            var km_hour = $("#km_hour option:selected").val();
            //finding object with speed index, chosen by user:
            var obj = speed_indexes.filter(function (item) {
                return item.speed == km_hour;
            });
            $("#speed_index option").removeAttr('selected');
            $("#speed_index option[value='" + obj[0].speed_index + "']").attr("selected", "selected");
            $("#speed_index").selectmenu('refresh', true);
        });

    }
    else
    {
        $("#loadFail div[role=main]").text("Ошибка загрузки данных для конвертера. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
        $.mobile.changePage("#loadFail", {role: "dialog"});
        $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
            .on("vmousedown", function (event) {
            $("#loadFail div[role=main]").text("");
            event.preventDefault();
            event.stopImmediatePropagation();
            $.mobile.changePage("#converters");
        });
    }
});

//clearing all selects, made by user:
$("#convert_speed").bind('pagehide', function () {
    $("#km_hour option").removeAttr('selected');
    $("#km_hour").selectmenu('refresh', true);
    $("#speed_index option").removeAttr('selected');
    $("#speed_index").selectmenu('refresh', true);
});

//converting load indexes:
$("#convert_load").bind('pageshow', function () {
    var load_indexes = localStorage.getItem("load_indexes");
    if (load_indexes)
    {
        load_indexes = JSON.parse(load_indexes);
        //arrays for lists of different types of speed indexes:
        var load_index_arr = [];
        var kg_arr = [];
        //populating arrays:
        for (var i = 0; i < load_indexes.length; i++)
        {
            load_index_arr.push(load_indexes[i].load_index);
            kg_arr.push(load_indexes[i].kg);
        }
        if($("#load_indexes option").length <= 1) {
            //adding lists from arrays to selects:
            $(load_index_arr).each(function () {
                $("<option/>").text(this).val(this).appendTo($("#load_indexes"));
            });
            $("#load_indexes").selectmenu('refresh', true);
        }
        if($("#loads option").length <= 1) {
            $(kg_arr).each(function () {
                $("<option/>").text(this).val(this).appendTo($("#loads"));
            });
            $("#loads").selectmenu('refresh', true);
        }
        //change event handlers for every select:
        $("#load_indexes").change(function(event) {
            event.stopImmediatePropagation();
            var load_index = $("#load_indexes option:selected").val();
            //finding object with load index, chosen by user:
            var obj = load_indexes.filter(function (item) {
                return item.load_index == load_index;
            });
            $("#loads option").removeAttr('selected');
            $("#loads option[value='" + obj[0].kg + "']").attr("selected", "selected");
            $("#loads").selectmenu('refresh', true);
        });

        $("#loads").change(function(event) {
            event.stopImmediatePropagation();
            var kg = $("#loads option:selected").val();
            //finding object with kg, chosen by user:
            var obj = load_indexes.filter(function (item) {
                return item.kg == kg;
            });
            $("#load_indexes option").removeAttr('selected');
            $("#load_indexes option[value='" + obj[0].load_index + "']").attr("selected", "selected");
            $("#load_indexes").selectmenu('refresh', true);
        });
    }
    else
    {
        $("#loadFail div[role=main]").text("Ошибка загрузки данных для конвертера. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
        $.mobile.changePage("#loadFail", {role: "dialog"});
        $("#loadFail div[data-role=header] a").off("vmousedown", "mousedown")
            .on("vmousedown", function (event) {
            $("#loadFail div[role=main]").text("");
            event.preventDefault();
            event.stopImmediatePropagation();
            $.mobile.changePage("#converters");
        });
    }
});

//clearing all selects, made by user:
$("#convert_load").bind('pagehide', function () {
    $("#loads option").removeAttr('selected');
    $("#loads").selectmenu('refresh', true);
    $("#load_indexes option").removeAttr('selected');
    $("#load_indexes").selectmenu('refresh', true);
});

