
function Truckforce(aName, aAddress, aPhone, aWorktime, aLat, aLng, aDist) {
    this.name = aName;
    this.address = aAddress;
    this.phone = aPhone;
    this.worktime = aWorktime;
    this.lat = aLat;
    this.lng = aLng;
    this.distance = aDist;
}


function Tire(aType, aSize, aInstal, aLoadIndex, aLoadMax, aPressure){
    this.transport_type = aType;
    this.tireSize = aSize;
    this.instal = aInstal;
    this.loadIndex = aLoadIndex;
    this.loadMax=aLoadMax;
    this.tirePressure = aPressure;
}

function calcRoute(dirService, dirRenderer, start, end) {
    var request =  {
        origin: start,
        destination: end,
        provideRouteAlternatives: false,
        travelMode: 'DRIVING',
        drivingOptions:
            {
                departureTime: new Date(),
                trafficModel: 'pessimistic'
            },
        unitSystem: google.maps.UnitSystem.METRIC,
        region: 'uk'
    };
    dirService.route(request, function (result, status) {
        if (status == 'OK') {
            dirRenderer.setDirections(result);
        }
        else
            console.log("direction error");
    });
}


function checkConnection() {
    var networkState = navigator.network.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';
    return (states[networkState]);
}


function adMobLauncher() {
    var admobid = {
        banner: 'ca-app-pub-5539665813113243/8220749812',
        interstitial: 'ca-app-pub-5539665813113243/9697483014'
    };

    if (! AdMob ) { alert( 'admob plugin not ready' ); return; }

    // this will create a banner on startup
    AdMob.createBanner( {
        adId: admobid.banner,
        position: AdMob.AD_POSITION.BOTTOM_CENTER,
        isTesting: false,
        overlap: false,
        offsetTopBar: false,
        bgColor: 'black',
        autoShow: true
    } );
    AdMob.showBanner(8);

}