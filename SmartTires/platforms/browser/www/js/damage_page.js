//damage page:
$("#damage").bind('pageshow', function () {
    var footerHeight = $("#damage>div[data-role=footer]").height();
    $("#damage>div[data-role=content]").css("padding-bottom", footerHeight + 10 +"px");

    //photo popup turning on on the 'damage' page:
    $(".photopopup").on({
        popupbeforeposition: function () {
            var maxHeight = $(window).height() - 60 + "px";
            $(".photopopup img").css("max-height", maxHeight);
        }
    });
});

